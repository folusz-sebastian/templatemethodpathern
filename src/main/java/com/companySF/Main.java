package com.companySF;

public class Main {
    public static void main(String[] args) {
        UsVacationPolicy usVacationPolicy = new UsVacationPolicy();
        usVacationPolicy.accrueVacation();

        EuVacationPolicy euVacationPolicy = new EuVacationPolicy();
        euVacationPolicy.accrueVacation();
    }
}
