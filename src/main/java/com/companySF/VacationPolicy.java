package com.companySF;

public abstract class VacationPolicy {
    public void accrueVacation() {
        alterForLegalMinimums();
        calculateBaseVacationHours();
        applyToPayroll();
    }

    abstract protected void alterForLegalMinimums();
    private void calculateBaseVacationHours() {
        System.out.println("calculateBaseVacationHours");
    }
    private void applyToPayroll() {
        System.out.println("applyToPayroll");
    }
}
